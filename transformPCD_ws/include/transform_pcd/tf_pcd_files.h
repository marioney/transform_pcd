#ifndef TF_PCD_FILES_H
#define TF_PCD_FILES_H


#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/exceptions.h>

// File managing
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <transform_pcd/conversions.h>

class tfPcdFiles
{
public:
    tfPcdFiles();
    ~tfPcdFiles();

    int process_folder(int pcd_folder_nr);

private:

    std::string get_date(bool get_hour);

    // File managment

    bool open_log_file();
    bool write_log_header();
    void store_info(const std::string date_str);

    ros::Publisher pc_out_publisher;
    ros::Timer timer_;

    geometry_msgs::TransformStamped Tf_pc;

    std::string pc_in_filename;
    std::string pc_out_frame_id;
    std::string pcd_filename;
    std::string data_log_filename;
    std::ofstream log_data_file;



};

#endif // TF_PCD_FILES_H
