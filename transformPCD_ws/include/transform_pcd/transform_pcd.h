#ifndef TRANSFORM_PCD_H
#define TRANSFORM_PCD_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/exceptions.h>

// Image managment
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// File managing
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <transform_pcd/conversions.h>


class TransformPCD
{

public:
    TransformPCD();
    ~TransformPCD();

    geometry_msgs::TransformStamped Tf_gps;
    geometry_msgs::TransformStamped Tf_imu;
    geometry_msgs::TransformStamped Tf_pc;

    cv_bridge::CvImagePtr cv_ptr;

    bool gps_active_;
    bool imu_active_;
    std::string pc_out_frame_id;
    std::string pcd_filename;
    ros::Time gps_stamp_, imu_stamp_;
    double timeout_;
    double first_northing, first_easting, first_altitude;
    std::string data_log_filename;
    std::ofstream log_data_file;




private:
    // ROS Callbacks

    void pc_callback(const sensor_msgs::PointCloud2::ConstPtr& pc_msg);
    void gps_callback(const sensor_msgs::NavSatFix::ConstPtr& gps_msg);
    void imu_callback(const sensor_msgs::Imu::ConstPtr& imu_msg);
    void image_callback    (const sensor_msgs::ImageConstPtr& msg);
    std::string get_date(bool get_hour);

    // File managment

    bool open_log_file();
    bool write_log_header();
    void store_info(const std::string date_str);

    void spin(const ros::TimerEvent &e);

    // ROS Subscribers and Publishers

    ros::Subscriber pc_subscriber;
    ros::Subscriber gps_subscriber;
    ros::Subscriber imu_subscriber;

    ros::Publisher pc_out_publisher;

    image_transport::ImageTransport* it_;
    image_transport::Subscriber image_subscriber;
    image_transport::Publisher image_pub_;


    ros::Timer timer_;


};

#endif // TRANSFORM_PCD_H
