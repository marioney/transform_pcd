#include <transform_pcd/tf_pcd_files.h>

tfPcdFiles::tfPcdFiles()
{
    ROS_INFO("[tfPcdFiles::tfPcdFiles] - Class builder");

    ros::NodeHandle priv_nh("~");
    ros::NodeHandle nh;
    std::string pc_topic;
    std::string pc_out_topic;
    std::string gps_topic;
    std::string imu_topic;
    std::string image_topic;
    double freq;



    priv_nh.param("pc_out_topic", pc_out_topic, std::string("pc_out"));
    priv_nh.param("pc_out_frame", pc_out_frame_id, std::string("pc_out"));
    priv_nh.param("pcd_read_folder", pc_in_filename, std::string("/home/mario/Dropbox/pcd/"));
    nh.param("pcd_store_folder", pcd_filename, std::string("/home/mario/pcd_files/"));
    priv_nh.param("freq", freq, 10.0);



    pc_out_publisher = nh.advertise<sensor_msgs::PointCloud2>(pc_out_topic, 1);

    data_log_filename = pcd_filename + "log.txt";

    Tf_pc.header.frame_id = std::string("base_link");

    Tf_pc.transform.rotation.w = 1.0;
    Tf_pc.transform.rotation.x = 0.0;
    Tf_pc.transform.rotation.y = 0.0;
    Tf_pc.transform.rotation.z = 0.0;
    Tf_pc.transform.translation.x =  0.0;
    Tf_pc.transform.translation.y = 0.0;
    Tf_pc.transform.translation.z = 0.0; //Gps Altitude is not good
    Tf_pc.child_frame_id = pc_out_frame_id;






    ROS_INFO("[tfPcdFiles::tfPcdFiles] - Initialization completed");

}


int tfPcdFiles::process_folder(int pcd_folder_nr)
{
    ROS_DEBUG("[TransformPCD::spin] - Spin function at time %f", ros::Time::now().toSec());

    // check which sensors are still active

    ros::Time filter_stamp;
    filter_stamp = ros::Time::now();
    Tf_pc.header.stamp = filter_stamp;
    if(pcd_folder_nr <= 15)
    {
        Tf_pc.transform.translation.y =  (float) (-1.0 * pcd_folder_nr);
        Tf_pc.transform.translation.x =  0.0;
        Tf_pc.transform.rotation.w = 1.0;
        Tf_pc.transform.rotation.z = 0.0;

    }
    else if (pcd_folder_nr <= 32)
    {
        Tf_pc.transform.translation.y =  (float) -1.0 * (32 - pcd_folder_nr);
        Tf_pc.transform.translation.x =  5.2;
        Tf_pc.transform.rotation.w = 0.0;
        Tf_pc.transform.rotation.z = 1.0;

    }



    pcl::PointCloud<pcl::PointXYZ>::Ptr pc_from_file (new pcl::PointCloud<pcl::PointXYZ>);

    std::string str_folder_name;
    char folder_nr_c_str[10];

    sprintf(folder_nr_c_str, "%d", pcd_folder_nr);
    str_folder_name = pc_in_filename + folder_nr_c_str + std::string("/") ;


    ROS_INFO ("[TransformPCD::process_folder] - Directory: %s", str_folder_name.c_str());

    DIR *dir;
    struct dirent *ent;

    int n_muestras;

    n_muestras = 0;

    if ((dir = opendir (str_folder_name.c_str())) != NULL)
    {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL)
        {
            if(ent->d_name[0] == '.')
            {
                continue;
            }


            ROS_INFO ("[TransformPCD::process_folder] - Reading file: %s", ent->d_name);
            std::string str_full_path;
            str_full_path =  str_folder_name + ent->d_name;
            //ROS_INFO ("[TransformPCD::process_folder] - Full path: %s", str_full_path.c_str());

            if (pcl::io::loadPCDFile<pcl::PointXYZ> (str_full_path, *pc_from_file) == -1) //* load the file
            {
                PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
                return -1;
            }
            else
            {
                n_muestras++;
                if (n_muestras == 2)
                {
                    sensor_msgs::PointCloud2 cloud_in;
                    sensor_msgs::PointCloud2 cloud_out;


//                    ROS_INFO("[TransformPCD::pc_callback] - Pos X = %f", Tf_pc.transform.translation.x);
                    ROS_INFO("[TransformPCD::pc_callback] - Pos Y = %f", Tf_pc.transform.translation.y);
//                    ROS_INFO("[TransformPCD::pc_callback] - Pos Z = %f", Tf_pc.transform.translation.z);
//                    ROS_INFO("[TransformPCD::pc_callback] - Rot W = %f", Tf_pc.transform.rotation.w);
//                    ROS_INFO("[TransformPCD::pc_callback] - Rot X = %f", Tf_pc.transform.rotation.x);
//                    ROS_INFO("[TransformPCD::pc_callback] - Rot Y = %f", Tf_pc.transform.rotation.y);
//                    ROS_INFO("[TransformPCD::pc_callback] - Rot Z = %f", Tf_pc.transform.rotation.z);




                    pcl::toROSMsg(*pc_from_file,cloud_in);

                    tf2::doTransform (cloud_in, cloud_out, Tf_pc);

                    pc_out_publisher.publish(cloud_out);

                    pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
                    pcl::PCLPointCloud2 pcl_vtk_cloud;

                    pcl::fromROSMsg(cloud_out,pcl_cloud);
                    pcl_conversions::toPCL(cloud_out,pcl_vtk_cloud);
                    std::string tmp_filename;
                    std::string vtk_filename;
                    std::string date_str;

                    date_str = get_date(true);
                    tmp_filename = pcd_filename + "olivos_";
                    tmp_filename = tmp_filename +  folder_nr_c_str;
                    //tmp_filename = tmp_filename + date_str;
                    vtk_filename = tmp_filename + ".vtk";
                    tmp_filename = tmp_filename + ".pcd";


                    try
                    {
                        pcl::io::saveVTKFile(vtk_filename, pcl_vtk_cloud);
                    }
                    catch (pcl::IOException e_vtk)
                    {
                        ROS_WARN("[TransformPCD::pc_callback] - Error in pcl::io::saveVTKFile : %s", e_vtk.what());
                    }

//                    try
//                    {
//                        pcl::io::savePCDFileASCII(tmp_filename, pcl_cloud);
//                    }
//                    catch (pcl::IOException e_pcd)
//                    {
//                        ROS_WARN("[TransformPCD::pc_callback] - Error in pcl::io::savePCDFileASCII : %s", e_pcd.what());
//                    }
                    store_info(date_str);
                }
            }
        }
        closedir (dir);

    }
    else
    {
        /* could not open directory */
        ROS_ERROR("[TransformPCD::process_folder] - Could not open directory");
        return -1;
    }


}

 std::string tfPcdFiles::get_date(bool get_hour)
 {
   std::time_t now;
   char the_date[12];

   the_date[0] = '\0';

   now = std::time(NULL);

   if (now != -1)
   {
     if(get_hour)
     {
       std::strftime(the_date, 12, "%H_%M_%S", std::localtime(&now));
     }
     else
     {
       std::strftime(the_date, 12, "%d_%m-%H_%M", std::localtime(&now));
     }
   }

   return std::string(the_date);
 }


 bool tfPcdFiles::open_log_file()
 {
     bool is_open;


     try
     {
         log_data_file.open(data_log_filename.c_str(), std::ios::app);
         is_open = true;
     }
     catch (std::ofstream::failure &writeErr)
     {
         ROS_ERROR("Exception occured when writing to a file - %s", writeErr.what());
         is_open = false;
     }
     return is_open;
 }

 bool tfPcdFiles::write_log_header()
 {
   if (open_log_file())
   {
     //ROS_INFO("[STOP SIM::write_log_header] Create Log data file Header");
     log_data_file << "Filename, ";
     log_data_file << "Robot_pos_x, ";
     log_data_file << "Robot_pos_y, ";
     log_data_file << "Robot_pos_z, ";
     log_data_file << "Robot_rot_w, ";
     log_data_file << "Robot_rot_x, ";
     log_data_file << "Robot_rot_y, ";
     log_data_file << "Robot_rot_z";
     log_data_file << "\n";
     log_data_file.close();
     return true;
   }
   return false;
 }

 void tfPcdFiles::store_info(const std::string date_str)
 {
     if (open_log_file())
     {
       std::string tmp_string;
       char buffer[100];
       // Time Stamp
       tmp_string =  std::string("olivos_") + date_str + std::string(", ");
       log_data_file << tmp_string.c_str();


       // Robot pos

       tmp_string.clear();
       sprintf(buffer, "%4f, %.4f, %.4f, ", Tf_pc.transform.translation.x, Tf_pc.transform.translation.y, Tf_pc.transform.translation.z);
       tmp_string = buffer;
       log_data_file << tmp_string.c_str();

       // Robot_rot
       tmp_string.clear();
       sprintf(buffer, "%4f, %.4f, %.4f, %.4f", Tf_pc.transform.rotation.w, Tf_pc.transform.rotation.x, Tf_pc.transform.rotation.y, Tf_pc.transform.rotation.z);
       tmp_string = buffer;
       log_data_file << tmp_string.c_str();

       log_data_file << "\n";
       log_data_file.close();

     }
     else
     {
       ROS_WARN("[TransformPCD::store_info] log data file not open");
     }
     log_data_file.flush();

 }



int main(int argc, char **argv){

  ros::init(argc, argv, "transform_pcd");

  ROS_INFO("[TransformPCD_node] - Start node");


  tfPcdFiles* my_tfPcdFiles;

  my_tfPcdFiles = new tfPcdFiles();


  for ( int pcd_folder_nr = 0; pcd_folder_nr <= 32; pcd_folder_nr++)
  {
      my_tfPcdFiles->process_folder(pcd_folder_nr);
      ros::spinOnce();
  }




  return  0;


}

