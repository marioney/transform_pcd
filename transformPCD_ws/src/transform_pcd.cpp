#include <transform_pcd/transform_pcd.h>

TransformPCD::TransformPCD()
    : imu_active_(false),
      gps_active_(false)
{
    ROS_INFO("[TransformPCD::TransformPCD] - Class builder");

    ros::NodeHandle priv_nh("~");
    ros::NodeHandle nh;
    std::string pc_topic;
    std::string pc_out_topic;
    std::string gps_topic;
    std::string imu_topic;
    std::string image_topic;
    double freq;


    priv_nh.param("pc_topic", pc_topic, std::string("/assembled_cloud"));
    priv_nh.param("pc_out_topic", pc_out_topic, std::string("pc_out"));
    priv_nh.param("gps_topic", gps_topic, std::string("/gps_node/latlong"));
    priv_nh.param("imu_topic", imu_topic, std::string("/imu/data"));
    priv_nh.param("image_topic", image_topic, std::string("/logitech_camera/image_raw"));
    priv_nh.param("pc_out_frame", pc_out_frame_id, std::string("pc_out"));
    priv_nh.param("sensor_timeout", timeout_, 10.2);
    nh.param("pcd_store_folder", pcd_filename, std::string("/home/mario/pcd_files/"));
    priv_nh.param("freq", freq, 10.0);

    it_ = new image_transport::ImageTransport(nh);

    pc_subscriber = nh.subscribe(pc_topic, 1, &TransformPCD::pc_callback, this);
    gps_subscriber = nh.subscribe(gps_topic, 1, &TransformPCD::gps_callback, this);
    imu_subscriber = nh.subscribe(imu_topic, 1, &TransformPCD::imu_callback, this);

    image_subscriber = it_->subscribe(image_topic, 1, &TransformPCD::image_callback, this);

    pc_out_publisher = nh.advertise<sensor_msgs::PointCloud2>(pc_out_topic, 1);

    data_log_filename = pcd_filename + "log.txt";

    timer_ = priv_nh.createTimer(ros::Duration(1.0/std::max(freq,1.0)), &TransformPCD::spin, this);


    ROS_INFO("[TransformPCD::TransformPCD] - Initialization completed");

}

TransformPCD::~TransformPCD()
{
    cv::destroyWindow("Image");
}

void TransformPCD::pc_callback(const sensor_msgs::PointCloud2::ConstPtr &pc_msg)
{
    ROS_INFO("[TransformPCD::pc_callback] - in callback");
    sensor_msgs::PointCloud2 cloud_in;
    sensor_msgs::PointCloud2 cloud_out;



    ROS_INFO("[TransformPCD::pc_callback] - Pos X = %f", Tf_pc.transform.translation.x);
    ROS_INFO("[TransformPCD::pc_callback] - Pos Y = %f", Tf_pc.transform.translation.y);
    ROS_INFO("[TransformPCD::pc_callback] - Pos Z = %f", Tf_pc.transform.translation.z);
    ROS_INFO("[TransformPCD::pc_callback] - Rot W = %f", Tf_pc.transform.rotation.w);
    ROS_INFO("[TransformPCD::pc_callback] - Rot X = %f", Tf_pc.transform.rotation.x);
    ROS_INFO("[TransformPCD::pc_callback] - Rot Y = %f", Tf_pc.transform.rotation.y);
    ROS_INFO("[TransformPCD::pc_callback] - Rot Z = %f", Tf_pc.transform.rotation.z);




    cloud_in = *pc_msg;
    tf2::doTransform (cloud_in, cloud_out, Tf_pc);

    pc_out_publisher.publish(cloud_out);

    pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
    pcl::PCLPointCloud2 pcl_vtk_cloud;

    pcl::fromROSMsg(cloud_out,pcl_cloud);
    pcl_conversions::toPCL(cloud_out,pcl_vtk_cloud);
    std::string tmp_filename;
    std::string vtk_filename;
    std::string image_filename;
    std::string date_str;

    date_str = get_date(true);
    tmp_filename = pcd_filename + "olivos_";
    tmp_filename = tmp_filename + date_str;
    vtk_filename = tmp_filename + ".vtk";
    image_filename = tmp_filename + ".png";
    tmp_filename = tmp_filename + ".pcd";



    try
    {
        pcl::io::saveVTKFile(vtk_filename, pcl_vtk_cloud);
    }
    catch (pcl::IOException e_vtk)
    {
        ROS_WARN("[TransformPCD::pc_callback] - Error in pcl::io::saveVTKFile : %s", e_vtk.what());
    }

    try
    {
        pcl::io::savePCDFileASCII(tmp_filename, pcl_cloud);
    }
    catch (pcl::IOException e_pcd)
    {
        ROS_WARN("[TransformPCD::pc_callback] - Error in pcl::io::savePCDFileASCII : %s", e_pcd.what());
    }

    try
    {
       cv::imwrite( image_filename, cv_ptr->image);
    }
    catch (cv::Exception e_img)
    {
        ROS_WARN("[TransformPCD::pc_callback] -  cv::imwrite : %s", e_img.what());
    }

    store_info(date_str);

}



void TransformPCD::gps_callback(const sensor_msgs::NavSatFix::ConstPtr &gps_msg)
{
    ROS_DEBUG("[TransformPCD::gps_callback] - in callback");
    if (gps_msg->status.status == sensor_msgs::NavSatStatus::STATUS_NO_FIX)
    {
        ROS_INFO("[TransformPCD::gps_callback] - No fix.");
        return;
    }
    gps_stamp_ = gps_msg->header.stamp ;

    if (gps_stamp_ == ros::Time(0))
    {
        return;
    }

    double northing, easting;
    std::string zone;

    gps_common::LLtoUTM(gps_msg->latitude, gps_msg->longitude, northing, easting, zone);

    geometry_msgs::Vector3 gps_pos;
    geometry_msgs::Quaternion gps_rot;

    gps_pos.x = easting;
    gps_pos.y = northing;
    gps_pos.z = gps_msg->altitude;

//    ROS_INFO("[TransformPCD::gps_callback] - Pos X = %f",  gps_pos.x);
//    ROS_INFO("[TransformPCD::gps_callback] - Pos Y = %f",  gps_pos.y);
//    ROS_INFO("[TransformPCD::gps_callback] - Pos Z = %f",  gps_pos.z);

    gps_rot.w = 1.0;
    gps_rot.x = 0.0;
    gps_rot.y = 0.0;
    gps_rot.z = 0.0;

    Tf_gps.header = gps_msg->header;
    Tf_gps.transform.translation = gps_pos;
    Tf_gps.transform.rotation = gps_rot;
    Tf_gps.child_frame_id = pc_out_frame_id;

     // Activate GPS
     if (!gps_active_)
     {
         gps_active_ = true;
         first_northing = northing;
         first_easting = easting;
         first_altitude = gps_msg->altitude;
         ROS_INFO("[TransformPCD::gps_callback] - GPS sensor activated");
     }
 }

void TransformPCD::imu_callback(const sensor_msgs::Imu::ConstPtr &imu_msg)
{

    ROS_DEBUG("[TransformPCD::imu_callback] - in callback");
    imu_stamp_ = imu_msg->header.stamp;
    if (imu_stamp_ == ros::Time(0))
    {
        return;
    }

    geometry_msgs::Vector3 imu_pos;
    geometry_msgs::Quaternion imu_rot;

    imu_pos.x = 0.0;
    imu_pos.y = 0.0;
    imu_pos.x = 0.0;

    imu_rot.w = imu_msg->orientation.w;
    imu_rot.x = imu_msg->orientation.x;
    imu_rot.y = imu_msg->orientation.y;
    imu_rot.z = imu_msg->orientation.z;

//    ROS_INFO("[TransformPCD::imu_callback] - Rot W = %f", imu_rot.w );
//    ROS_INFO("[TransformPCD::imu_callback] - Rot X = %f", imu_rot.x );
//    ROS_INFO("[TransformPCD::imu_callback] - Rot Y = %f", imu_rot.y );
//    ROS_INFO("[TransformPCD::imu_callback] - Rot Z = %f", imu_rot.z );

    Tf_imu.header = imu_msg->header;
    Tf_imu.transform.translation = imu_pos;
    Tf_imu.transform.rotation = imu_rot;
    Tf_imu.child_frame_id = pc_out_frame_id;

    // Activate IMU
    if (!imu_active_)
    {
        imu_active_ = true;
        ROS_INFO("[TransformPCD::imu_callback] - IMU sensor activated");
    }
}


void TransformPCD::image_callback(const sensor_msgs::ImageConstPtr &msg)
{

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

//    // Update GUI Window
    cv::imshow("Image", cv_ptr->image);
    cv::waitKey(3);

}

// filter loop
 void TransformPCD::spin(const ros::TimerEvent& e)
 {
     ROS_DEBUG("[TransformPCD::spin] - Spin function at time %f", ros::Time::now().toSec());

     // check for timing problems
     if (gps_active_ && imu_active_)
     {
         double diff = fabs(ros::Duration(gps_stamp_ - imu_stamp_).toSec() );
         if (diff > 1.0)
         {
             ROS_ERROR("[TransformPCD::spin] - Timestamps of odometry and imu are %f seconds apart.", diff);
         }
     }

     // check which sensors are still active
     ros::Time filter_stamp;

     filter_stamp = ros::Time::now();

//     if ( gps_active_ &&
//          ((filter_stamp.toSec() - gps_stamp_.toSec()) > timeout_))
//     {
//         gps_active_ = false;
//         ROS_INFO("[TransformPCD::spin] - GPS sensor not active any more");
//     }
//     if ( imu_active_ &&
//          ((filter_stamp.toSec() - imu_stamp_.toSec()) > timeout_))
//     {
//         imu_active_ = false;
//         ROS_INFO("[TransformPCD::spin] - Imu sensor not active any more");
//     }

     // only update when two sensors are active
     if (imu_active_ && gps_active_)
     {

//         ROS_INFO("[TransformPCD::spin] - Pos X = %f", Tf_gps.transform.translation.x);
//         ROS_INFO("[TransformPCD::spin] - Pos Y = %f", Tf_gps.transform.translation.y);
//         ROS_INFO("[TransformPCD::spin] - Pos Z = %f", Tf_gps.transform.translation.z);

//         ROS_INFO("[TransformPCD::spin] - Rot W = %f", Tf_imu.transform.rotation.w );
//         ROS_INFO("[TransformPCD::spin] - Rot X = %f", Tf_imu.transform.rotation.x );
//         ROS_INFO("[TransformPCD::spin] - Rot Y = %f", Tf_imu.transform.rotation.y );
//         ROS_INFO("[TransformPCD::spin] - Rot Z = %f", Tf_imu.transform.rotation.z );

         Tf_pc.header.frame_id = Tf_gps.header.frame_id;
         Tf_pc.header.stamp = filter_stamp;
         Tf_pc.transform.rotation = Tf_imu.transform.rotation;
         Tf_pc.transform.translation.x = Tf_gps.transform.translation.x - first_easting;
         Tf_pc.transform.translation.y = Tf_gps.transform.translation.y - first_northing;
         Tf_pc.transform.translation.z = 0.0; //Gps Altitude is not good
         Tf_pc.child_frame_id = pc_out_frame_id;
     }

 }

 std::string TransformPCD::get_date(bool get_hour)
 {
   std::time_t now;
   char the_date[12];

   the_date[0] = '\0';

   now = std::time(NULL);

   if (now != -1)
   {
     if(get_hour)
     {
       std::strftime(the_date, 12, "%H_%M_%S", std::localtime(&now));
     }
     else
     {
       std::strftime(the_date, 12, "%d_%m-%H_%M", std::localtime(&now));
     }
   }

   return std::string(the_date);
 }


 bool TransformPCD::open_log_file()
 {
     bool is_open;


     try
     {
         log_data_file.open(data_log_filename.c_str(), std::ios::app);
         is_open = true;
     }
     catch (std::ofstream::failure &writeErr)
     {
         ROS_ERROR("Exception occured when writing to a file - %s", writeErr.what());
         is_open = false;
     }
     return is_open;
 }

 bool TransformPCD::write_log_header()
 {
   if (open_log_file())
   {
     //ROS_INFO("[STOP SIM::write_log_header] Create Log data file Header");
     log_data_file << "Filename, ";
     log_data_file << "Robot_pos_x, ";
     log_data_file << "Robot_pos_y, ";
     log_data_file << "Robot_pos_z, ";
     log_data_file << "Robot_rot_w, ";
     log_data_file << "Robot_rot_x, ";
     log_data_file << "Robot_rot_y, ";
     log_data_file << "Robot_rot_z";
     log_data_file << "\n";
     log_data_file.close();
     return true;
   }
   return false;
 }

 void TransformPCD::store_info(const std::string date_str)
 {
     if (open_log_file())
     {
       std::string tmp_string;
       char buffer[100];
       // Time Stamp
       tmp_string =  std::string("olivos_") + date_str + std::string(", ");
       log_data_file << tmp_string.c_str();


       // Robot pos

       tmp_string.clear();
       sprintf(buffer, "%4f, %.4f, %.4f, ", Tf_pc.transform.translation.x, Tf_pc.transform.translation.y, Tf_pc.transform.translation.z);
       tmp_string = buffer;
       log_data_file << tmp_string.c_str();

       // Robot_rot
       tmp_string.clear();
       sprintf(buffer, "%4f, %.4f, %.4f, %.4f", Tf_pc.transform.rotation.w, Tf_pc.transform.rotation.x, Tf_pc.transform.rotation.y, Tf_pc.transform.rotation.z);
       tmp_string = buffer;
       log_data_file << tmp_string.c_str();

       log_data_file << "\n";
       log_data_file.close();

     }
     else
     {
       ROS_WARN("[TransformPCD::store_info] log data file not open");
     }
     log_data_file.flush();

 }
